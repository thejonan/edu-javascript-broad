function receiveBox(box) {
	console.log("Box received: " + JSON.stringify(box));

	var boxEl = $("#" + box.id);

	if (!boxEl.length) {
		boxEl = $("#new-box")
			.clone()
			.prop('id', box.id)
			.appendTo('#main-container');
	} 

	// Update all the fields
	boxEl.css('width', box.width)
		.css('height', box.height)
		.css('background-color', box.color)
		.css('left', box.x)
		.css('top', box.y);

	$('.name', boxEl[0]).html(box.id);
	$('.message', boxEl[0]).html(box.text);
}

var socket = io(),
		myID = 12321;

$(() => {
	// The "All Done jQuery" function
	$("#command").keypress(() => {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			// The jQuery wrapped DOM element.
			var me = $(event.target);
			
			// Send the command to the web server.
			$.post('do', { 'cmd': me.val() }); 
			
			// Reset the value after the command is sent.
			me.val(""); 
		}
	});

	$.get('list', (data) => { 
		for (var id in data)
			receiveBox(data[id]);
	});
})

socket.on('box', receiveBox);