var express = require('express'),
		bodyParser = require('body-parser'),
		app = express(),
		http = require('http').Server(app),
		io = require('socket.io')(http),
		Box = require('./box.js');

// Tell the ExpressJS web server that we're going to use static files from
// this, very same directory.
app.use(express.static(__dirname));

// This one makes the express web server parse the passed body properly
app.use(bodyParser.urlencoded({extended: false}))

/************* Now we start with the routing - definition of endpoints */
app.get('/list', (req, res) => {
	console.log("All boxes requested...");
	res.send(commands.listAll()); 
});

app.post('/do', async (req, res) => {
	try {
		console.log("New command arrived: [" + req.body.cmd + "]");
		var box = commands.parse(req.body.cmd);
		io.emit('box', box.wrap());
		res.sendStatus(200);
	} 
	catch (error) {
		res.sendStatus(500);
		return console.log('error',error);
	} 
	finally {
		console.log('Command done');
	}
});

io.on('connection', () => {
	console.log('A user is connected')
})

/*********** The actual start of the web server */
var server = http.listen(3000, () => {
	console.log('Server is running on port', server.address().port);
});

/********* The commands processing object - an example of "static" methods. */
var commands = {
	cmdRegExp: /^\/(\w+):(\w+)(\s+.*)?$/m,
	
	parse(command) {
		var parsed = command.match(this.cmdRegExp);
		if (!parsed)
			throw "Wrong command sent: " + command;

		var cmd = parsed[1].toLowerCase(),
				id = parsed[2],
				args = (parsed[3] || "").trim().split(/\s/),
				theBox = null;

		if (cmd === "new") {
			theBox = new Box(id, args);
		} else if (cmd === "message") {
			// This requires special attention because of the spaces
			theBox = Box.all[id];
			theBox.message(parsed[3] || "");
		} else {
			// The normal command-direct-propagation mode
			theBox = Box.all[id];
			// TODO: The notion of `this` here!
			theBox[cmd].apply(theBox, args);
		}

		return theBox;
	},
	
	listAll() {
		return Box.all 
	}
};