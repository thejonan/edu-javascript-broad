var allBoxes = {};

module.exports = Box;

function Box(id, args) {
    this.id = id;

    this.x = Math.random() * 800;
    this.y = Math.random() * 600;
    this.width = parseInt(args[0]);
    this.height = parseInt(args[1]);

    this.color = "red";
    this.text = "---";

    allBoxes[id] = this;
    console.log("New box w/ ID: " + id + " @ " + this.width + "x" + this.height + " created!");
};

Box.all = allBoxes;

/** Now, these are the actual "methods" of the Box prototype. */
Box.prototype.wrap = function () {
    return this;
};

Box.prototype.move = function (dx, dy) {
    this.x += parseInt(dx);
    this.y += parseInt(dy);
};

Box.prototype.paint = function (color) {
    this.color = color;
};

Box.prototype.message = function (text) {
    this.text = text;
};